import json
from bdalchemist.default import indexers
from config import src_folder, goslated_folder, dest_folder
from bdalchemist.alchemy import wb_dict
from openpyxl import load_workbook
import os


def merge_folder(src_folder=src_folder,
                 goslated_folder=goslated_folder,
                 dest_folder=dest_folder,
                 language_pref=('en', 'ru')):
    if not os.path.exists(dest_folder):
        os.makedirs(dest_folder)
    for filename in os.listdir(src_folder):
        src_path = os.path.join(src_folder, filename)
        goslated_path = os.path.join(goslated_folder, filename)
        dest_path = os.path.join(dest_folder, filename)
        if not os.path.exists(goslated_path):
            continue

        # load files
        xl_workbook = load_workbook(src_path)
        workbook = wb_dict(xl_workbook, indexers)
        goslated_data = json.load(open(goslated_path, 'r'))

        # merge
        for language_key in language_pref:
            language_data = goslated_data.get(language_key, {})
            for key, values in language_data.iteritems():
                wb_value = workbook.get(key)
                if key is None:
                    continue
                kr_value = values['kr']
                if kr_value != wb_value.value:
                    continue
                lan_value = values.get(language_key, None)
                if not lan_value:
                    print "bad, %s" % key
                    continue
                wb_value.value = lan_value
        xl_workbook.save(dest_path)


if __name__ == '__main__':
    merge_folder(src_folder, goslated_folder, dest_folder)