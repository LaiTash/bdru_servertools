from openpyxl import load_workbook
from openpyxl.cell import Cell
from default import indexers
import logging


def wb_dict(wb, indexers):
    result = {}
    for sheetname in wb.sheetnames:
        sheet = wb.get_sheet_by_name(sheetname)
        rows = sheet.iter_rows()
        columns = [c.value for c in next(rows)]
        for row in rows:
            index = indexers.get(sheetname, indexers['default'])(row)
            index = '%s!%s' % (sheetname, index)
            for cell_i, cell in enumerate(row):
                column = columns[cell_i]
                if len(columns) > 2:
                    result['%s%s' % (index, column)] = cell
                else:
                    result[index] = cell
    return result


def merge(dest, src, filters=None):
    merged = 0
    skipped = 0
    for key, cell in src.iteritems():
        if cell is None:
            logging.log(2, 'cell is None')
            continue
        dest_cell = dest.get(key, None)
        if dest_cell is None:
            logging.log(2, "%s not found in destination" % key)
            continue
        if isinstance(cell, Cell):
            value = cell.value
        elif isinstance(cell, list):
            value = '\n\n'.join([c.encode('utf-8') for c in cell])
        else:
            value = cell.encode('utf-8')
        if filters and all((f(dest_cell.value, value, key) for f in filters)):
            dest_cell.value = value
            merged += 1
    logging.log(2, 'Total merged: %i' % merged)


def merge_files(dest, src, output, indexers=indexers):
    dest_wb = load_workbook(dest)
    src_wb = load_workbook(src)
    dest_dict = wb_dict(dest_wb, indexers)
    src_dict = wb_dict(src_wb, indexers)
    merge(dest_dict, src_dict)
    dest_wb.save(output)