import re

__author__ = 'Lai'


def filter_numeric(dest_cell, src_cell, key):
    dest_nums = list(sorted(map(int, re.findall('\d+', dest_cell))))
    src_nums = list(sorted(map(int, re.findall('\d+', src_cell))))
    return dest_nums == src_nums


def filter_equality(kr_data):
    def _wr(dest_cell, src_cell, key):
        kr_value = kr_data.get([key], None)
        if kr_value is None:
            return True
        return kr_value == dest_cell.value
    return _wr