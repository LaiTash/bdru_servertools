import os
import re
from config import combined_folder, dest_folder
from openpyxl import load_workbook, Workbook
from bdalchemist.alchemy import wb_dict

COMBINE = {
    'LanguageData': 'LanguageData.xlsm',
    '^stringtable_kr': 'stringtable_ru.xlsm',
    'symbolno': 'symbolnostringtable_ru.xlsm'
}

def combine_files():
    combined_files = {key: Workbook() for key in COMBINE.iterkeys()}

    if not os.path.exists(combined_folder):
        os.makedirs(combined_folder)
    for filename in os.listdir(dest_folder):
        tmp_filepath = os.path.join(dest_folder, filename)
        tmp_wb = load_workbook(tmp_filepath)
        cmb_wb = next([wb for key, wb in combined_files.iteritems()
                       if re.match(key, filename)])
        for sheetname in tmp_wb.sheetnames:
            tmp_sheet = tmp_wb.get_sheet_by_name(sheetname)
            tmp_rows = tmp_sheet.iter_rows()
            row0 = next(tmp_rows)
            if not tmp_sheet in cmb_wb.sheetnames:
                cmb_sheet = cmb_wb.create_sheet(sheetname)
                cmb_sheet.append(row0)
            else:
                cmb_sheet = cmb_wb.get_sheet_by_name(sheetname)
            for row in tmp_rows:
                cmb_sheet.append([cell.value for cell in row])
    for key, fn in COMBINE.iteritems():
        fp = os.path.join(combined_folder, fn)
        combined_files[key].save(fp)


if __name__ == '__main__':
    combine_files()