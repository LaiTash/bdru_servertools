indexers = {
    'default': lambda row: unicode(row[0].value),
    'Dialog_Table': lambda row: '%s^%s^%s'
                                % (row[0].value, row[1].value, row[2].value),
    'Quest_Table': lambda row: '%s^%s'% (str(row[0].value), str(row[1].value)),
    'Skill_Table_New': lambda row: '%s^%s' % (row[0].value, row[1].value),

}

omits = {
    'default': (0,),
    'Dialog_Table': (0, 1, 2),
    'Quest_Table': (0, 1,),
    'Skill_Table_New': (0, 1)
}


dict_indexers = {
    'default': lambda sheet, row: unicode(row[sheet['columns'][0]]),
    'Dialog_Table':
        lambda sheet, row: '%s_%s_%s'
                           % map(unicode,
                                 (row['cells']['!Npc'],
                                  row['cells']['!DialogIndex'],
                                  row['cells']['^Index'])),
    'Quest_Table:': lambda sheet, row: '%s_%s'
                                       % map(unicode,
                                             (row['cells']['^QuestGroup'],
                                              row['cells']['^QuestID'])),
    'Skill_Table_New':
        lambda sheet, row:
        '%s_%s' % map(unicode,
                      (row['cells']['^SkillNo'], row['cells']['^SkillLev']))
}



def lxml2dict(wb, include_cell=False):
    result = {}
    for sheetname in wb.sheetnames:
        sheet_table = {}
        sheet = wb.get_sheet_by_name(sheetname)
        rows = sheet.iter_rows()
        index_row = next(rows)
        indices = [c.value for c in index_row]
        for row_i, row in enumerate(rows):
            std_index = indexers.get(sheetname, indexers['default'])(row)
            row_table = {}
            for cell_i, cell in enumerate(row):
                if include_cell:
                    row_table[indices[cell_i]] = cell
                else:
                    row_table[indices[cell_i]] = cell.value
            row_table['xl_position'] = row_i
            sheet_table[std_index] = row_table
        result[sheetname] = sheet_table
    return result