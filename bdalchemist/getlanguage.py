#coding=utf-8

import string

EN_CHARS = string.letters + string.digits + 'абвгдеёжзийклмнопрстуфхцчъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧЪЫЬЭЮЯ'

def get_language(string_, factor=0.7):
    if not string_:
        return 'kr'
    #ru = len(filter(RU_CHARS.__contains__, string_))
    en = len(filter(EN_CHARS.__contains__, string_))
    if float(en) / len(string_) < factor:
        return 'kr'
    return 'en'