import json
import logging
import os

from openpyxl import load_workbook

from bdalchemist.alchemy import wb_dict, merge
from bdalchemist.default import indexers
from bdalchemist.filters import filter_numeric
from config import src_folder, dest_folder, onesky_folder


destinations = [
    'LanguageData.xlsm',
    'stringtable_ru.xlsm',
    'symbolnostringtable_ru.xlsm',
]


def merge_onesky():
    for folder in (src_folder, dest_folder, onesky_folder):
        if not os.path.exists(folder):
            os.makedirs(folder)
    for filename, fileext in map(os.path.splitext, os.listdir(dest_folder)):
        dest_fp = os.path.join(dest_folder, filename + fileext)
        onesky_fp = os.path.join(onesky_folder, filename + '.json')
        if not os.path.exists(onesky_fp):
            continue
        dest_wb = load_workbook(dest_fp)
        dest_dict = wb_dict(dest_wb, indexers)
        data = json.load(open(onesky_fp, 'r'))
        translation = data.get('ru', None)
        if not translation:
            continue
        translation = translation['translation']
        merge(dest_dict, translation, [filter_numeric])
        dest_wb.save(dest_fp)


def merge_onesky_():
    for folder in (src_folder, dest_folder, onesky_folder):
        if not os.path.exists(folder):
            os.makedirs(folder)

    for destination in destinations:
        logging.getLogger('OneskyMerger').info("Merging %s" % destination)
        save_to = os.path.join(dest_folder, destination)
        load_from = os.path.join(src_folder, destination)

        dest = load_workbook(load_from)
        dest_dict = wb_dict(dest, indexers)

        for filename in os.listdir(onesky_folder):
            data = json.load(open(os.path.join(onesky_folder, filename), 'r'))
            translation = data.get('ru')
            if not translation:
                continue
            translation = translation['translation']
            merge(dest_dict, translation, [filter_numeric])

        dest.save(save_to)


if __name__ == '__main__':
    merge_onesky()