import logging
from onesky.client import Client
import time
import os
from config import onesky_folder, onesky_secret_key, onesky_api_key, \
    onesky_project_id

onesky_relative_url = 'projects/{}/translations/multilingual'.format(
    onesky_project_id)
file_format =  'I18NEXT_MULTILINGUAL_JSON'


def download_onesky():
    logger = logging.getLogger('OneskyDownloader')
    client = Client(onesky_api_key, onesky_secret_key)
    client.download_dir = onesky_folder
    files = client.file_list(onesky_project_id)[1]['data']
    total_started = time.time()
    if not os.path.exists(client.download_dir):
        os.makedirs(client.download_dir)
    for file_data in files:
        file_name = file_data['file_name']
        logger.info("Downloading %s" % file_name)
        dest_file_name = os.path.splitext(file_name)[0] + '.json'
        # client.quotation_show()
        #raw_input(client.translation_status(project_id, file_name, 'ru'))
        #continue
        dnld_started = time.time()
        resp = client.do_http_request(onesky_relative_url,
                                      {'source_file_name': file_name,
                                       'export_file_name': dest_file_name,
                                       'file_format': file_format})
        logger.info("Download time: %i seconds" % (time.time() - dnld_started))
    print('Total time: %i seconds' % (time.time() - total_started))


if __name__ == '__main__':
    download_onesky()