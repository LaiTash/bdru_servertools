import os
from bdalchemist.alchemy import wb_dict, merge
from bdalchemist.default import indexers
from config import merge_folder, dest_folder
from openpyxl import load_workbook
from bdalchemist.filters import filter_numeric


def merge_sources():
    if not os.path.exists(merge_folder):
        return
    for tmp_filename in os.listdir(dest_folder):
        dest_filepath = os.path.join(dest_folder, tmp_filename)
        dest_wb = load_workbook(dest_filepath)
        dest_dict = wb_dict(dest_wb, indexers)
        for folder, filenames in ((w[0], w[2]) for w in os.walk(merge_folder)):
            for filename in filenames:
                fp = os.path.join(folder, filename)
                merge_dict = wb_dict(load_workbook(fp), indexers)
                merge(dest_dict, merge_dict, [filter_numeric])
        dest_wb.save(dest_filepath)


if __name__ == '__main__':
    merge_sources()