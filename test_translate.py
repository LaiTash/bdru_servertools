import json
import re
from bdalchemist.default import indexers
from bdalchemist.goslator import translate, TranslationRule
from bdalchemist.alchemy import wb_dict
from openpyxl import load_workbook
import logging
import logging.config
import os
from config import src_folder, goslated_folder


def key_regexp(table, column):
    return re.compile("%s![^~]+%s" % (table, column))


STD_REPLACE = re.compile('[^a-zA-Z()<>#&{}-]+')

class STD_RULE(TranslationRule):
    replace_regexp = STD_REPLACE

TRANSLATION_RULES = {

    # Quests
    TranslationRule(
        key_regexp=key_regexp('Quest_Table', '~CompleteCondition'),
        replace_regexp=re.compile('[^\dA-Za-z();,.\n\t]+'),
        cleanup=lambda s: re.sub('[;,.()]', '', s)),
    STD_RULE(key_regexp('Quest_Table', '~Desc')),
    STD_RULE(key_regexp('Quest_Table', '~Title')),
    STD_RULE(key_regexp('Quest_Table', '~AcceptDialog')),
    STD_RULE(key_regexp('Quest_Table', '~AcceptButton'), language='ru'),
    STD_RULE(key_regexp('Quest_Table', '~ProgressDialog')),
    STD_RULE(key_regexp('Quest_Table', '~RetryButton'), language='ru'),
    STD_RULE(key_regexp('Quest_Table', '~CompleteDialog')),

    # Items
    STD_RULE(key_regexp('Item_Table', '~ItemName'), language='ru'),
    STD_RULE(key_regexp('Item_Table', '~ItemName')),
    STD_RULE(key_regexp('Item_Table', '~Description')),
    STD_RULE(key_regexp('Item_Table', '~PopupDesc')),

    # Characters
    STD_RULE(key_regexp('Character_Table', '~CharName'), language='ru'),
    STD_RULE(key_regexp('Character_Table', '~DisplayName'), language='ru'),
    STD_RULE(key_regexp('Character_Table', '~CharName')),
    STD_RULE(key_regexp('Character_Table', '~DisplayName')),


    # Cards
    STD_RULE(key_regexp('Card_Table', '~Name')),
    STD_RULE(key_regexp('Card_Table', '~Name'), language='ru'),
    STD_RULE(key_regexp('Card_Table', '~Desc')),
    STD_RULE(key_regexp('Card_Table', '~Keyword'), language='ru'),
    STD_RULE(key_regexp('Card_Table', '~Keyword')),

    #
}


if not os.path.exists(goslated_folder):
    os.makedirs(goslated_folder)


#def get_percents(dest, data):
#    tmp_data = data.get('en', {})
#    tmp_data.update(data.get('ru', {}))
#    print len(tmp_data)


def translate_file(filepath, out_path):
    if os.path.splitext(filepath)[1] not in ('.xlsx', '.xlsm'):
        return
    logging.getLogger('Translation').info(
        'Loading source workbook: %s' % filepath)
    source_wb = load_workbook(filepath)
    source_wb = wb_dict(source_wb, indexers)
    #print source_wb.keys()
    logging.getLogger('Translation').info('Translation started')
    if not os.path.exists(out_path):
        data = {}
    else:
        data = json.load(open(out_path, 'r'))
    translated_n = [0]
    #get_percents(source_wb, data)
    def callback(key):
        translated_n[0] += 1
        if translated_n[0] == 300:
            translated_n[0] = 0
            json.dump(data, open(out_path, 'w'))
        #get_percents(source_wb, data)
    translate(source_wb, data, 'en', TRANSLATION_RULES, callback)
    translate(source_wb, data, 'ru', TRANSLATION_RULES, callback)
    json.dump(data, open(out_path, 'w'))


def translate_folder(original_folder, goslated_folder):
    for filename in os.listdir(original_folder):
        filepath = os.path.join(original_folder, filename)
        out_path = os.path.join(goslated_folder, filename)
        translate_file(filepath, out_path)


if __name__ == '__main__':
    logging.config.fileConfig('logging.conf')
    translate_folder(src_folder, goslated_folder)