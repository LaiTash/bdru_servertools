import logging
import re
from time import sleep
from traceback import print_exc
from goslate import Goslate
from openpyxl.cell import Cell


class TranslationRule(object):
    key_regexp = re.compile('.+')
    replace_regexp = re.compile('.+', re.DOTALL)
    cleanup = lambda self, s: s
    language = 'en'

    def __init__(self, key_regexp=None, replace_regexp=None, cleanup=None,
                 language=None):
        self.key_regexp = key_regexp or self.key_regexp
        self.replace_regexp = replace_regexp or self.replace_regexp
        self.cleanup = cleanup or self.cleanup
        self.language = language or self.language


DEFAULT_RULES = [TranslationRule]


def translate_record(goslate, record, language):
    value = record.group()
    if value and len(value) > 1 and not value.isdigit():
        print value
        return goslate.translate(record.group(), language, 'kr')
    else:
        return value



def translate(source, data, language='en', rules=DEFAULT_RULES, callback=None,
              wait=1):
    '''

    :param source: Source dictionary
    :param data: Path to translation data
    :param goslate: Goslate object
    :param regexp: Regular expression to translate
    :param cleanup: Function to apply to translation result
    :param language: 'ru', 'en', etc
    :return: translation dictionary
    '''
    goslate = Goslate()
    if language not in data:
        data[language] = {}
    for key, value in source.iteritems():
        if isinstance(value, Cell):
            value = value.value
        data_value = data[language].get(key)
        if data_value and data_value['kr'] == value:
            continue
        result = value
        for rule in rules:
            if rule.language != language or not rule.key_regexp.match(key):
                continue
            try:
                result = re.sub(rule.replace_regexp, lambda s: rule.cleanup(
                    translate_record(goslate, s, language)), result)
            except Exception as e:
                print_exc()
                msg = 'exception at %s: %s, waiting %i seconds to continue'
                logging.getLogger('Translation').error(msg % (key, e, wait))
                sleep(wait)
                logging.getLogger('Translation').info('Translation resumed')
        if result == value:
            continue
        data[language][key] = {'kr': value, language: result}
        if callback:
            callback(key)