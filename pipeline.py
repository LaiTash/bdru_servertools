import logging
import os
import combine_files
import goslate_merger
import merge_sources
import onesky_dnld
import onesky_merger
from bdalchemist.alchemy import wb_dict, merge, merge_files
from config import combined_folder, final_folder, stencil_folder


def stencil_merge():
    for filename in os.listdir(combined_folder):
        comb_filepath = os.path.join(combined_folder, filename)
        final_path = os.path.join(final_folder, filename)
        stencil_path = os.path.join(stencil_folder, filename)
        if not (os.path.exists(comb_filepath)
                and os.path.exists(stencil_path)):
            continue
        merge_files(stencil_path, comb_filepath, final_path)


logger = logging.getLogger('root')

if __name__ == '__main__':
    logger.info('Downloading Translations')
    onesky_dnld.download_onesky()
    logger.info('Meging goslations')
    goslate_merger.merge_folder()
    logger.info('Meging onesky')
    onesky_merger.merge_onesky()
    logger.info('Meging sources')
    merge_sources.merge_sources()
    logger.info('Combining')
    combine_files.combine_files()
    logger.info('Meging stencils')
    stencil_merge()
